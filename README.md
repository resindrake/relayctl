# Relay Controller
Controls a relay using an IR remote

## Pinouts:
| Pin | Mode   | Purpose              |
| --- | ------ | -------------------- |
| 1   | Output | Relay Controller Pin |
| 2   | Input  | IR Sensor Pin        |

When the play/pause button on the remote is pushed, the state of the relay pin is toggled.
