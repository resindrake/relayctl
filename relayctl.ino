  #include <DigiCDC.h>
//#include <IRremote.h>

const int tickrate = 10; // Tick duration (ms)

// Pins
const int relay_pin = 1; // Digital output - controls the relay
const int ir_pin = 2; // Digital input - receives infrared signals

// Variables
int relay_state = LOW;
char rx_byte = 0;

// Buttons
const int button_playpause = 0x00FFC23D;

// Object stuff idk
//IRrecv irrecv(ir_pin);
//decode_results results;

//int get_button() {
//  int out = 0x00000000;
//  if (irrecv.decode(&results)) {
//    out = results.value;
//    irrecv.resume();
//  }
//  return out;
//}

void setup() {
  SerialUSB.begin();
  pinMode(relay_pin, OUTPUT);
  pinMode(ir_pin, INPUT);
}

void loop() {
  //if (get_button() == button_playpause) toggle_relay();

  if (SerialUSB.available() > 0) {    // is a character available?
    rx_byte = -1;
    rx_byte = SerialUSB.read();       // get the character
    switch (rx_byte) {
      case 'f':
        relay_state = LOW;          // Turn relay off
        break;
      case 'n':
        relay_state = HIGH;         // Turn relay on
        break;
      case 't':
        relay_state = !relay_state; // Toggle relay
        break;
    }
    SerialUSB.println(relay_state);
  }
  
  digitalWrite(relay_pin, relay_state);
  SerialUSB.delay(tickrate);
}
